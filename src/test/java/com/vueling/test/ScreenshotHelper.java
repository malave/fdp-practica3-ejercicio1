package com.vueling.test;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class ScreenshotHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static void screenshot(WebDriver driver, String name) {
        String currentPath = System.getProperty("user.dir");
        Path path = java.nio.file.Paths.get(currentPath, "screenshots");
        String filename = path.toString() + File.separator + "screenshot-" + name + ".png";
        LOGGER.debug(filename);
        try {
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            InputStream initialStream = new FileInputStream(scrFile);
            File targetFile = new File(filename);
            java.nio.file.Files.copy(initialStream, targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            LOGGER.debug("TakesScreenshot error", e);
        }

    }
}
