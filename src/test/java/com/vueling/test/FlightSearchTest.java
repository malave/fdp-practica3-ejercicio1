package com.vueling.test;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

public class FlightSearchTest {

    private WebDriver driver;
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static final String URL = "https://www.vueling.com/es";
    private static final String VALUE_FROM = "Barcelona";
    private static final String VALUE_TO = "Valencia";
    private static final String VALUE_HEADER = "Barcelona (BCN) - Valencia (VLC)";
    private static final String ID_ACCEPT_COOKIES = "ensCloseBanner";
    private static final String SELECTOR_FROM = "#tab-search > div > div.form-group.form-group--flight-search > vy-airport-selector.form-input.origin > div > input";
    private static final String SELECTOR_FROM_OPTION = "#popup-list > vy-airports-li > li > p:nth-child(1)";
    private static final String SELECTOR_TO = "#tab-search > div > div.form-group.form-group--flight-search > vy-airport-selector.form-input.destination > div > input";
    private static final String SELECTOR_TO_OPTION = "#popup-list > vy-airports-li > li.liStation > p:nth-child(1)";
    private static final String SELECTOR_ONE_WAY_OPTION = "#searchbar > div > vy-datepicker-popup > vy-datepicker-header > ul > li:nth-child(2) > label";
    private static final String SELECTOR_DATE = "#searchbar > div > vy-datepicker-popup > vy-specificdates-datepicker > div > div.ui-datepicker-group.ui-datepicker-group-last > table > tbody > tr:nth-child(1) > td:nth-child(6) > a"; //May 1 2021
    private static final String ID_SEARCH_BUTTON = "btnSubmitHomeSearcher";
    private static final String SELECTOR_HEADER_TITLE = "#newStv > div.stv-journey > div > div.vy-journey_header > h3";


    @BeforeClass
    public static void setupWebdriverChromeDriver() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
    }

    @Before
    public void setup() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void teardown() {
        LOGGER.debug("Tear Down");
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    public void searchForFlight() {
        LOGGER.debug("searchForFlight - Start");
        driver.get(URL);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(ID_ACCEPT_COOKIES))).click();
        ScreenshotHelper.screenshot(driver, "1-initial");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(SELECTOR_FROM))).sendKeys(VALUE_FROM);
        ScreenshotHelper.screenshot(driver, "2-from-selection");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(SELECTOR_FROM_OPTION))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(SELECTOR_TO))).sendKeys(VALUE_TO);
        ScreenshotHelper.screenshot(driver, "3-to-selection");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(SELECTOR_TO_OPTION))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(SELECTOR_ONE_WAY_OPTION))).click();
        ScreenshotHelper.screenshot(driver, "4-date-selection");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(SELECTOR_DATE))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(ID_SEARCH_BUTTON))).click();
        WebElement header = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(SELECTOR_HEADER_TITLE)));
        ScreenshotHelper.screenshot(driver, "5-result-page");
        assertThat(header.getText(), containsString(VALUE_HEADER));
    }
}
