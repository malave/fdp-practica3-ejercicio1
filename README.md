# Setup
- Download [chromedriver](https://chromedriver.chromium.org/downloads)  for your OS
- Put in the root of the poject folder with the name `chromedriver`

# Testing
Run `gradle test`

In the folder `./build/reports/tests/test/index.html` you will find the report created by JUnit and on the folder `./screenshots` you will find the screenshots takes during testing.

